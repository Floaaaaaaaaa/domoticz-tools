-- Florent PASQUER - 2014
-- Ce script permet :
-- - l'envoi d'un email + declenchement sirene quand le detecteur de mouvement a changÃ© de valeur et est en mode 'dÃ©tection'.
-- - l'envoi de l'ip externe du raspberry pi 2 fois par jour
print('________________________________________________ BOF______ SCRIPT script_device_raspberrytemp.lua')
-- print('________________ impression otherdevices')
-- for i, v in pairs(otherdevices) do
--	print(i..' => '..v)
-- end
-- print('________________ impression otherdevices_svalues')
-- for i, v in pairs(otherdevices_svalues) do
-- print(i..' => '..v)
-- end
-- print('________________ impression devicechanged')
-- for i, v in pairs(devicechanged) do
-- print(i..' => '..v)
-- end


--------------------------------
------ BOF Variables Ã  Ã©diter ------
--------------------------------
local sondeDetecteurMouvement = 'DetecteurMouvement' -- Nom de la sonde responsable de la dectection de mouvement
local sondeTemperature = 'Temperature' -- Nom de la sonde responsable de la temperature
local alarmeLevel = 'AlarmeLevel' -- Nom du dimmer responsable de l'activation de l'alarme
local intAlarmeMail = 'Alarme_Mail' -- Nom de l'interrupteur responsable de l'envoi de mail en cas de mouvement
local intEnvoiIP = 'Envoi_IP' -- Nom de l'interrupteur responsable de l'envoi d'ip externe du raspberry
local intReboot = 'Reboot' -- Nom de l'interrupteur responsable du reboot du système
local intAlarmeSireneInstantanee = 'Alarme_Sirene_Instantanee' -- Nom de l'interrupteur responsable de l'activation de l'alarme par un user
local intAlarmeSireneDetection = 'Alarme_Sirene_Detection' -- Nom de l'interrupteur responsable de l'activation de l'alarme suite a mouvement
local intAlarmeModeTest = 'Alarme_Mode_Test' -- Nom de l'interrupteur responsable du mode test de l'alarme c'est a dire que des lumieres et pas de son
--------------------------------
------ EOF Variables Ã  Ã©diter ------
--------------------------------

commandArray = {}


local isAlarmeSireneDetection = otherdevices[intAlarmeSireneDetection] == 'On'
local isAlarmeSireneInstantanee = otherdevices[intAlarmeSireneInstantanee] == 'On'
local isAlarmeModeTest = otherdevices[intAlarmeModeTest] == 'On'
local isAlarmeMail = otherdevices[intAlarmeMail] == 'On'
local isEnvoiIP = otherdevices[intEnvoiIP] == 'On' and devicechanged[intEnvoiIP]
local isReboot = otherdevices[intReboot] == 'On' and devicechanged[intReboot]
local handle = io.popen("curl http://ipecho.net/plain")
local ipAddress = handle:read("*a")
local ipAddressMessage = '<br>Adresse Ip : http://'..ipAddress..':8080'
local ipAddressCommand = 'Ip Address#'..ipAddressMessage..'.#florent.pasquer@gmail.com'
handle:close()

if (isReboot) then
	os.execute('reboot');
end

if (isEnvoiIP) then
	print ('ipAddress'..ipAddress)
	commandArray['SendEmail']=ipAddressCommand
end

if (devicechanged[intAlarmeSireneInstantanee]) then
  	print('__________________ Alarme sirene instantanee changed')
  	if (isAlarmeSireneInstantanee) then
    		print('________________ Activation alarme')
                if (isAlarmeModeTest) then
			commandArray[alarmeLevel..'']='Set Level 33'
		else
    			commandArray[alarmeLevel..'']='Set Level 66'
    			commandArray['SendEmail']='Alarme Sonore#Attention alarme sonore en action.'..ipAddressMessage..'.#florent.pasquer@gmail.com'
		end
  	else
    		print('________________ Desactivation alarme sonore')
    		commandArray[alarmeLevel..'']='Set Level 0'
  	end
end
 

-- print('_________debut____________________')
-- print('mouvement other____________________')
-- print(otherdevices[sondeDetecteurMouvement])
-- print('mouvement value____________________')
-- print(otherdevices_svalues[sondeDetecteurMouvement])
-- print('temperature other____________________')
-- print(otherdevices[sondeTemperature])
-- print('temperature value____________________')
-- print(otherdevices_svalues[sondeTemperature])
-- print('alarme level value____________________')
-- print(otherdevices[alarmeLevel..''])
-- print(otherdevices_svalues[alarmeLevel..''])
-- print('_________fin____________________')

if ((isAlarmeSireneDetection or isAlarmeMail)) then
	if (devicechanged[sondeDetecteurMouvement]) then
		local value = otherdevices[sondeDetecteurMouvement] -- Valeur relevÃ©e dans le motion
		if (value == 'On') then
                        if (isAlarmeMail) then
				print('________________ Activation envoi email')
				commandArray['SendEmail']='Alarme#Attention mouvement dÃ©tectÃ© sur la sonde : '..sondeDetecteurMouvement..' (valeur : '..value..')'..ipAddressMessage..'.#florent.pasquer@gmail.com'
			end
			if (isAlarmeSireneDetection) then
				print('________________ Activation alarme sonore')
				commandArray[intAlarmeSireneInstantanee..'']='On'
			end
		end
	end
end
print('________________________________________________ EOF______ SCRIPT script_device_raspberrytemp.lua')
return commandArray