-- Ce script vérifie la présence de x téléphones sur le réseau pour savoir si quelqu'un est là.
-- La vérification est effectuée une fois par minute tant qu'aucun téléphone n'est détecté,
-- puis une fois toute les 10 minutes quand au moins un téléphone est connecté pour ne pas trop stresser sa batterie.
 
commandArray = {}
 
--Cette fonction calcule la différence de temps (en secondes) entre maintenant
--et la date passée en paramètre.
function timedifference (s)
  year = string.sub(s, 1, 4)
  month = string.sub(s, 6, 7)
  day = string.sub(s, 9, 10)
  hour = string.sub(s, 12, 13)
  minutes = string.sub(s, 15, 16)
  seconds = string.sub(s, 18, 19)
  t1 = os.time()
  t2 = os.time{year=year, month=month, day=day, hour=hour, min=minutes, sec=seconds}
  difference = os.difftime (t1, t2)
  return difference
end

local intPresenceConnue = 'Presence_Connue'
local majInSec = 600
 
--Si la présence n'est pas détectée ou que la présence n'a pas été testée depuis plus de 10 minutes (600 secondes),
--alors on vérifie à nouveau sa présence
if (otherdevices[intPresenceConnue]=='Off' or (otherdevices[intPresenceConnue]=='On' and timedifference(otherdevices_lastupdate[intPresenceConnue]) > majInSec)) then
	ping_success_tel1=os.execute('ping -c1 192.168.1.59') -- FLORENT
	ping_success_tel2=os.execute('ping -c1 192.168.1.11') -- HELENE

	if ping_success_tel1 or ping_success_tel2 then
	  commandArray[intPresenceConnue]='On'
	else
	  if otherdevices[intPresenceConnue]=='On' then --On ne passe l'interrupteur virtuel à Off que s'il est sur On.
             commandArray[intPresenceConnue]='Off'
          end
	end
end
 
return commandArray
